package com.appdev.bilous.androidschedule;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import java.util.ArrayList;

public class NureContentChoose extends AppCompatActivity {

    private Context m_context;
    private String m_tag = "MyLog";
    private Intent m_nextIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nure_content_choose);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        m_context = this;
        UniversityChooseThread thread = new UniversityChooseThread();
        thread.start();

    }

    private class UniversityChooseThread extends Thread {

        private int m_chooseFlag =-1;

        @Override
        public void run() {
            ListView contentChooseListView = new ListView(m_context);
            ArrayList<String> contentArray = new ArrayList<>();
            contentArray.add("Группы");
            contentArray.add("Преподаватели");
            contentArray.add("Аудитории");
            ArrayAdapter<String>contentChooseArrayAdapter = new ArrayAdapter<String>(m_context, android.R.layout.simple_list_item_1, contentArray);
            contentChooseListView.setAdapter(contentChooseArrayAdapter);
            contentChooseListView.setLayoutParams(new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, 130 * contentArray.size()));
            contentChooseListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.content_choose_lin);
            linearLayout.addView(contentChooseListView);

            while (true) {
                if (contentChooseListView.getCheckedItemPosition() != m_chooseFlag) {
                    try {
                        switch (contentChooseListView.getCheckedItemPosition()) {
                            case 0:
                                Log.d(m_tag, contentChooseListView.getAdapter().getItem(0).toString());
                                m_chooseFlag = 0;
                                throw new GroupException();
                            case 1:
                                Snackbar.make(contentChooseListView, "К сожалению рассписание преподавателей ещё не готово", Snackbar.LENGTH_LONG)
                                        .show();
                                m_chooseFlag = 1;
                                break;
                            case 2:
                                Snackbar.make(contentChooseListView, "К сожалению рассписание аудиторий ещё не готово", Snackbar.LENGTH_LONG)
                                        .show();
                                m_chooseFlag = 2;
                                break;
                        }
                    } catch (GroupException e) {
                        m_nextIntent = new Intent(m_context, NureFacultyChoose.class);
                        m_nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        m_nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        m_nextIntent.putExtra("selected", "0");
                        Log.d(m_tag, "Starting nure groop choose activity");
                        break;
                    }
                }
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    Log.d(m_tag, e.toString());
                }
            }
            startActivity(m_nextIntent);
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void onBackPressed() {
        Intent backIntent = new Intent(this,UniversityChoose.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(backIntent);
    }

    private class GroupException extends Exception {}
    private class TeacherException extends Exception {}
    private class CabinetException extends Exception {}

}
