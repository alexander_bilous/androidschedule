package com.appdev.bilous.androidschedule;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;

public class StartActivity extends AppCompatActivity {

    private Context m_context;
    private Intent m_intent;
    private String m_tag = "My_log";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        m_context = this;
        Thread nextPathChoose = new NextPathChoose();
        nextPathChoose.start();
    }

    private class NextPathChoose extends Thread {
        @Override
        public void run() {
            File file = new File(m_context.getFilesDir(), "ids.txt");
            if (file.exists()) {
                Log.d(m_tag, "Saved groups found, starting schedule activity.");
                FileInputStream inputStream;
                byte[] bytes = null;
                try {
                    inputStream = new FileInputStream(file);
                    bytes = new byte[inputStream.available()];
                    inputStream.read(bytes);
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                String savedIds = new String(bytes);
                savedIds.hashCode();
            } else {
                Log.d(m_tag, "No saved group found, starting University choose activity.");
                m_intent = new Intent(m_context, UniversityChoose.class);
                m_intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                m_intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(m_intent);
            }
        }
    }
}
