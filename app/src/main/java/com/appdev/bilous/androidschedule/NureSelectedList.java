package com.appdev.bilous.androidschedule;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;

public class NureSelectedList extends AppCompatActivity {

    private Intent m_intent;
    private Context m_context;
    private LinearLayout m_linearLayout;
    private String m_tag = "Mylog";
    private String m_faculty;
    private ArrayList<String> m_groupList;
    private HashMap<String, String> m_keys;
    private HashMap<String, String> m_groupIds;
    private LinkedHashSet<String> m_groupSet;
    private boolean m_flag = false;
    private ProgressBar progressBar;
    private Intent m_nextIntent;


    @Override
    public void onBackPressed() {
        Intent nextIntent = new Intent(m_context, NureFacultyChoose.class);
        nextIntent.putExtra("faculties", m_intent.getStringArrayListExtra("faculties"));
        nextIntent.putExtra("selected", m_intent.getStringExtra("selected"));
        nextIntent.putExtra("keys", m_intent.getSerializableExtra("keys"));
        nextIntent.putExtra("fromFuture", true);
        nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(nextIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nure_selected_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        m_intent = getIntent();
        m_context = this;
        m_linearLayout = (LinearLayout) findViewById(R.id.selected_list_lin);
        m_faculty = m_intent.getStringExtra("faculty");
        m_keys = (HashMap) m_intent.getSerializableExtra("keys");
        m_groupIds = new HashMap<>();
        m_groupSet = new LinkedHashSet<>();

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.NureSelectedList_coordinatorLayout);
        progressBar = new ProgressBar(m_context);
        progressBar.setPadding(dpToPx(170), dpToPx(270), 0, 0);
        coordinatorLayout.addView(progressBar);
        getSupportActionBar().setTitle("Загрузка данных");

        if (m_intent.getStringExtra("selected").equals("0")) {
            new GroupParce().execute();
            GroupThread groupThread = new GroupThread();
            groupThread.start();
        } else if (m_intent.getStringExtra("selected").equals("1")) ;
        else ;
    }

    private class GroupThread extends Thread {
        @Override
        public void run() {

            while (true) {
                if (m_flag) {
                    final ListView groupChooseListView = new ListView(m_context);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                             m_groupList = new ArrayList<String>();
                            for (String x : m_groupSet)
                                m_groupList.add(x);
                            Collections.sort(m_groupList);
                            ArrayAdapter<String> groupChooseArrayAdapter = new ArrayAdapter<String>(m_context, android.R.layout.simple_list_item_1, m_groupList);
                            groupChooseListView.setAdapter(groupChooseArrayAdapter);
                            groupChooseListView.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.WRAP_CONTENT, 130 * m_groupSet.size()));
                            groupChooseListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.selected_list_lin);
                            linearLayout.addView(groupChooseListView);
                            if (progressBar != null) progressBar.setVisibility(View.GONE);
                            getSupportActionBar().setTitle("Выберите группу: ");
                        }
                    });

                    while (true) {
                        if (groupChooseListView.getCheckedItemPosition() != -1) {

                            File file = new File(m_context.getFilesDir(), "ids.txt");
                            if (file.exists()) {
                                try {
                                    FileOutputStream fos = openFileOutput("ids.txt", Context.MODE_APPEND);
                                    String selectedGroupName = m_groupList.get(groupChooseListView.getCheckedItemPosition());
                                    String willBeWritten = "0 "+selectedGroupName+" "+m_groupIds.get(selectedGroupName)+" ";
                                    fos.write(willBeWritten.getBytes());
                                    fos.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } else
                            {
                                try {
                                    file.createNewFile();
                                    FileOutputStream fos = openFileOutput("ids.txt", Context.MODE_APPEND);
                                    String selectedGroupName = m_groupList.get(groupChooseListView.getCheckedItemPosition());
                                    String willBeWritten = "0 "+selectedGroupName+" "+m_groupIds.get(selectedGroupName)+" \n";
                                    fos.write(willBeWritten.getBytes());
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                                //    m_nextIntent = new Intent(m_context, NureSelectedList.class);
                                //    m_nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                //   m_nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                break;
                        }
                        try {
                            sleep(100);
                        } catch (InterruptedException e) {
                            Log.d(m_tag, e.toString());
                        }

                    }
                    break;
                }
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // startActivity(m_nextIntent);
            Thread.currentThread().interrupt();
        }
    }

    private int dpToPx(int dp) {
        DisplayMetrics displayMetrics = m_context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private class GroupParce extends AsyncTask<Void, Void, String> {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson1 = "";

        @Override
        protected String doInBackground(Void... params) {
            //getting data from url server
            try {
                URL url = new URL("http://cist.nure.ua/ias/app/tt/P_API_GROUP_JSON");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream, "cp1251"));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                resultJson1 = buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                reader.close();
            } catch (IOException e) {
                Log.d(m_tag, e.getMessage());
            }
            return resultJson1;
        }

        @Override
        protected void onPostExecute(String strJSON1) {
            super.onPostExecute(strJSON1);
            JSONObject dataJsonObj = null;

            try {
                dataJsonObj = new JSONObject(strJSON1);
                Log.d(m_tag, "Group Parce Starting");

                String facultyId = m_keys.get(m_faculty);

                JSONObject university = dataJsonObj.getJSONObject("university");
                JSONArray faculties = university.getJSONArray("faculties");
                int faculties_length = faculties.length();
                for (int i = 0; i < faculties_length; i++) {
                    JSONObject temp = faculties.getJSONObject(i);
                    if (facultyId.equals(temp.optString("id"))) {
                        JSONArray temp_arr = temp.getJSONArray("directions");
                        int _temp_arr_length = temp_arr.length();
                        for (int j = 0; j < _temp_arr_length; j++) {
                            JSONObject obj = temp_arr.getJSONObject(j);
                            JSONArray jsarr = obj.getJSONArray("specialities");
                            int _jsarr_length = jsarr.length();
                            for (int n = 0; n < _jsarr_length; n++) {
                                JSONObject final_obj = jsarr.getJSONObject(n);
                                JSONArray final_array = final_obj.getJSONArray("groups");
                                int _final_array_length = final_array.length();
                                for (int m = 0; m < _final_array_length; m++) {
                                    JSONObject group_list = final_array.getJSONObject(m);
                                    m_groupSet.add(group_list.getString("name"));
                                    m_groupIds.put(group_list.getString("name"), group_list.getString("id"));
                                }
                            }

                        }
                    }

                }

                Log.d(m_tag, "Group Parce Done");

                m_flag = true;
                Log.d(m_tag, "Thread flag is true now");

            } catch (Exception e) {
                Log.d(m_tag, e.getMessage());
            }
        }
    }
}

