package com.appdev.bilous.androidschedule;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class NureFacultyChoose extends AppCompatActivity {

    private Context m_context;
    private String m_tag = "MyLog";
    private Intent m_nextIntent;
    private ArrayList<String> m_facultiesList;
    private HashMap<String, String> m_keys;
    private Intent m_intent;
    private boolean m_flag = false;
    private ProgressBar progressBar;


    @Override
    public void onBackPressed() {
        Intent backIntent = new Intent(this,UniversityChoose.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(backIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nure_faculty_choose);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        m_context = this;
        m_facultiesList = new ArrayList<>();
        m_keys = new HashMap<>();
        m_intent = getIntent();

        if (!m_intent.getBooleanExtra("fromFuture", false)) {
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.facultychose_coordinatorLayout);
            progressBar = new ProgressBar(m_context);
            progressBar.setPadding(dpToPx(170), dpToPx(270), 0, 0);
            coordinatorLayout.addView(progressBar);
            getSupportActionBar().setTitle("Загрузка данных");

            new FacultiesList().execute();
            SecondaryThread secondaryThread = new SecondaryThread();
            secondaryThread.start();
        } else {
            m_facultiesList = m_intent.getStringArrayListExtra("faculties");
            m_keys = (HashMap) m_intent.getSerializableExtra("keys");
            m_flag = true;
            SecondaryThread secondaryThread = new SecondaryThread();
            secondaryThread.start();
        }

    }

    private int dpToPx(int dp) {
        DisplayMetrics displayMetrics = m_context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private class SecondaryThread extends Thread {

        @Override
        public void run() {
            while (true) {
                if (m_flag) {
                    final ListView contentChooseListView = new ListView(m_context);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            ArrayAdapter<String> contentChooseArrayAdapter = new ArrayAdapter<String>(m_context, android.R.layout.simple_list_item_1, m_facultiesList);
                            contentChooseListView.setAdapter(contentChooseArrayAdapter);
                            contentChooseListView.setLayoutParams(new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, 130 * m_facultiesList.size()));
                            contentChooseListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.nure_faculty_lin);
                            linearLayout.addView(contentChooseListView);
                            if(progressBar!=null)  progressBar.setVisibility(View.GONE);
                            getSupportActionBar().setTitle("Выберите факультет: ");
                        }
                    });


                    while (true) {
                        if (contentChooseListView.getCheckedItemPosition() != -1) {

                            m_nextIntent = new Intent(m_context, NureSelectedList.class);
                            m_nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            m_nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            m_nextIntent.putExtra("selected", m_intent.getStringExtra("selected"));
                            m_nextIntent.putExtra("faculty", m_facultiesList.get(contentChooseListView.getCheckedItemPosition()));
                            m_nextIntent.putExtra("keys", m_keys);
                            m_nextIntent.putExtra("faculties", m_facultiesList);
                            break;
                        }
                        try {
                            sleep(100);
                        } catch (InterruptedException e) {
                            Log.d(m_tag, e.toString());
                        }

                    }
                    break;
                }
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            startActivity(m_nextIntent);
            Thread.currentThread().interrupt();
        }
    }

    private class FacultiesList extends AsyncTask<Void, Void, String> {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String resultJson = "";

        @Override
        protected String doInBackground(Void... params) {
            //getting data from url server
            try {
                URL url = new URL("http://cist.nure.ua/ias/app/tt/P_API_PODR_JSON");
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream inputStream = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                reader = new BufferedReader(new InputStreamReader(inputStream, "cp1251"));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);

                }
                resultJson = buffer.toString();
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.d(m_tag, e.getMessage());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return resultJson;
        }

        @Override
        protected void onPostExecute(String strJSON) {
            super.onPostExecute(strJSON);

            JSONObject dataJsonObj = null;

            try {

                dataJsonObj = new JSONObject(strJSON);
                Log.d(m_tag, "Structure Parce Starting");
                JSONObject university = dataJsonObj.getJSONObject("university");
                JSONArray _faculties = university.getJSONArray("faculties");
                int _faculties_length = _faculties.length();
                for (int i = 0; i < _faculties_length; i++) {
                    JSONObject temp = _faculties.getJSONObject(i);
                    m_facultiesList.add(temp.getString("full_name"));
                    m_keys.put(temp.getString("full_name"), temp.getString("id"));
                }
                Log.d(m_tag, "Structure Parce Done");
                m_flag = true;

            } catch (Exception e) {
                Log.d(m_tag, e.getMessage());
            }


        }
    }
}
