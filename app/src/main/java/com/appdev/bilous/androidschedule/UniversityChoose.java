package com.appdev.bilous.androidschedule;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;

import java.util.ArrayList;

public class UniversityChoose extends AppCompatActivity {

    private Context m_context;
    private String m_tag = "MyLog";
    private int m_chooseFlag = -1;
    private Intent m_nextIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university_choose);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        m_context = this;
        Thread universityChoose = new UniversityChooseThread();
        universityChoose.start();
    }

    private class UniversityChooseThread extends Thread {
        @Override
        public void run() {
            ListView universityListView = new ListView(m_context);
            ArrayList<String> univercityArray = new ArrayList<>();
            univercityArray.add("ХНУРЭ");
            univercityArray.add("ХАИ");
            ArrayAdapter<String> univercityArrayAdapter = new ArrayAdapter<String>(m_context, android.R.layout.simple_list_item_1, univercityArray);
            universityListView.setAdapter(univercityArrayAdapter);
            universityListView.setLayoutParams(new ScrollView.LayoutParams(ScrollView.LayoutParams.MATCH_PARENT, 130 * univercityArray.size()));
            universityListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.uni_choose_lin);
            linearLayout.addView(universityListView);

            while (true) {
                if (universityListView.getCheckedItemPosition() != m_chooseFlag) {
                    try {
                        switch (universityListView.getCheckedItemPosition()) {
                            case 0:
                                Log.d(m_tag, universityListView.getAdapter().getItem(0).toString());
                                m_chooseFlag = 0;
                                throw new NureException();
                            case 1:
                                Snackbar.make(universityListView, "К сожалению рассписание ещё не готово", Snackbar.LENGTH_LONG)
                                        .show();
                                m_chooseFlag = 1;
                                break;
                        }
                    } catch (NureException e) {
                        m_nextIntent = new Intent(m_context, NureContentChoose.class);
                        m_nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        m_nextIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        Log.d(m_tag, "Starting nure content choose activity");
                        break;
                    }
                }
                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    Log.d(m_tag, e.toString());
                }
            }
            startActivity(m_nextIntent);
        }
    }

    private class NureException extends Exception {
    }

    private class HaiException extends Exception {
    }

}
